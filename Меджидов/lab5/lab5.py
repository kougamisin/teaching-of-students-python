# Написать метод,
# выполняющие деление и выдающий исключение в случае,
# когда делитель равен нулю.
def division(a, b):
    try:
        a = float(a)
        b = float(b)
        return a / b
    except ZeroDivisionError: # деление на ноль
        print("Деление на ноль")
    except ValueError:  # хотя бы одна переменная не числова
        print("Не получается привести к числовому типу")



# Написать метод,
# складывающий две переменные,
# каждая из которых может быть либо числом,
# либо строкой,
# и переводящий число в строку
# в случае несоответствия типов.
def add(a, b):
    try:
        return a + b
    except TypeError:  # хотя бы одна переменная не числова
        return str(a) + str(b)



# Написать метод для проверки того,
# что с клавиатуры ввели отрицательное целое число.
# Создать свой класс исключений.

class NegNumError(Exception):			#собственный класс исключений
    def __init__(self, text):
        self.txt = text

class NotIntError(Exception):			#собственный класс исключений
    def __init__(self, text):
        self.txt = text

def is_int(val):
    try:
        return type(val) == int or val.is_integer()
    except AttributeError:
        return False

def is_neg(num):
    try:
        num = float(num)
    except ValueError:  # хотя бы одна переменная не числова
        raise ValueError("Не получается привести к числовому типу.")
    else:
        if not is_int(num):
            raise NotIntError("Число не целое.")
        elif int(num) < 0:
            raise NegNumError("Число отрицательное.")
        return int(num)